import boto3
ddb = boto3.client("dynamodb")
from ask_sdk_core.skill_builder import SkillBuilder
from ask_sdk_core.dispatch_components import AbstractRequestHandler
from ask_sdk_core.dispatch_components import AbstractExceptionHandler
from ask_sdk_core.utils import is_request_type, is_intent_name

class LaunchRequestHandler (AbstractRequestHandler):
    def can_handle (self, handler_input):
        return is_request_type ('LaunchRequest')(handler_input)
    
    def handle (self, handler_input):
        handler_input.response_builder.speak ('Oi, eu sou o Iave! Como posso te ajudar?').set_should_end_session(False)
        return handler_input.response_builder.response
    
class CatchAllExceptionHandler (AbstractExceptionHandler):
    def can_handle(self, handler_input, exception):
        return True
    
    def handle (self, handler_input, exception):
        print (exception)
        handler_input.response_builder.speak ('Desculpa, tive um problema')
        return handler_input.response_builder.response

class QtdeParqueDesligIntentHandler (AbstractRequestHandler):
    def can_handle (self, handler_input):
        return is_intent_name ('QtdeParqueDesligIntent')(handler_input)
    
    def handle (self, handler_input):
        ontem = handler_input.request_envelope.request.intent.slots['ontem'].value
        try:
            data = ddb.get_item(
                TableName="resumodesligamento",
                Key={
                    'Data': {
                        'S': ontem
                    }
                }
            )
        except BaseException as e:
            print(e)
            raise(e)
       
        
        speech_text = 'Deixa eu ver. Eu encontrei '+data['Item']['Qtde_desligamentos']['S']+' desligamento.'

        #speech_text = 'A geração líquida de ontem foi de '+data['Item']['Liquida']['S']+' mega watts, com '+data['Item']['Fator de Capacidade']['S']+' de fator de capacidade!'
        handler_input.response_builder.speak(speech_text).set_should_end_session(False)
        return handler_input.response_builder.response
    
sb = SkillBuilder ()
sb.add_request_handler (LaunchRequestHandler())
sb.add_exception_handler (CatchAllExceptionHandler())
sb.add_request_handler (QtdeParqueDesligIntentHandler())

def handler (event, context):
    return sb.lambda_handler()(event, context)